# OpenWindow App
OpenWindow is an Android app crafted for  ecologically challenged areas where there might be air pollution or sand intrusion into homes at specific wind directions. With it, you'll be informed when it's safe to open the window and enjoy fresh air.
![home](/images/1_163.png) ![settings](/images/2_163.png) ![forecast](/images/3_163.png)

## Technologies:
* Java
* Gradle
* Android Studio

## Features:
1. **Wind Direction Parsing**: The application analyses data from a website and provides the user with current wind direction and hourly forecast for the day.
2. **Safety Recommendations**: Based on the analyzed data, the application offers recommendations on whether it's safe to open the window.
3. **Flexibility in Setting Up**: The application can easily be adapted to any region by merely changing two links (for the current forecast and the hourly forecast), as well as the main condition.
4. **Sound and vibration settings**: Users can enable sound and vibration for button presses.
5. **Quick Sound and Vibration Turn Off**: With a single button, all can be quickly turned off.
6. **Multilingual**: The application is displayed in the language of the user's device. The interface supports several languages.

### How to Use:
* Launch the application and after a couple of seconds you will see the wind direction and a hint whether it is safe to open the window.
* Click on the cloud icon to view the basis for the decision.
* Press the right button on the navigation menu to view the daily forecast.
* Go to settings to adjust sound or vibration.