package com.example.openwindow.settings.sound;

import android.media.AudioManager;
import android.media.ToneGenerator;

public class SoundUtils {
    public static void playSound() {
        ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);
        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP, 200);
    }
}
