package com.example.openwindow.settings.sound;

public interface SoundCallback {
    void playSound();
}
