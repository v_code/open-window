package com.example.openwindow;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.openwindow.footer.FooterFragment;
import com.example.openwindow.footer.FooterUpdater;
import com.example.openwindow.settings.vibration.VibrationUtils;
import com.example.openwindow.settings.sound.SoundCallback;

public class HeaderFragment extends Fragment {

    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_header, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View settingsIcon = view.findViewById(R.id.header_icon);
        TextView headerText = view.findViewById(R.id.header_text);
        headerText.setText(getString(R.string.header_text));

        settingsIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", Context.MODE_PRIVATE);
                boolean soundEnabled = sharedPreferences.getBoolean("sound_enabled", false);
                if (soundEnabled) {
                    ((SoundCallback) getActivity()).playSound();
                }

                boolean vibrationEnabled = sharedPreferences.getBoolean("vibration_enabled", false);
                if (vibrationEnabled) {
                    VibrationUtils.vibratePhone(getContext());
                }
                handleHeaderIconClick();
            }
        });
    }

    private void handleHeaderIconClick() {
        FragmentManager fragmentManager = getParentFragmentManager();
        SettingsFragment settingsFragment = new SettingsFragment();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, settingsFragment);
        fragmentTransaction.commit();

        if(getActivity() instanceof FooterUpdater) {
            ((FooterUpdater) getActivity()).updateFooterState(FooterFragment.FragmentState.SETTINGS);
        }
    }
}