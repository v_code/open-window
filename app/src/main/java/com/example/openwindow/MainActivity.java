package com.example.openwindow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.openwindow.footer.FooterFragment;
import com.example.openwindow.footer.FooterUpdater;
import com.example.openwindow.settings.sound.SoundCallback;
import com.example.openwindow.settings.sound.SoundUtils;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements FooterUpdater, SoundCallback {

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment headerFragment = new HeaderFragment();
        fragmentManager.beginTransaction().replace(R.id.header_container, headerFragment).commit();

        Fragment firstScreenFragment = new FirstScreenFragment();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, firstScreenFragment).commit();

        Fragment footerFragment = new FooterFragment();
        fragmentManager.beginTransaction().replace(R.id.footer_container, footerFragment).commit();
    }

    @Override
    public void updateFooterState(FooterFragment.FragmentState state) {
        FooterFragment footerFragment = (FooterFragment) getSupportFragmentManager().findFragmentById(R.id.footer_container);
        if(footerFragment != null) {
            footerFragment.setCurrentFragmentState(state);
            footerFragment.updateIcons();
        }
    }

    @Override
    public void playSound() {
        SoundUtils.playSound();
    }
}