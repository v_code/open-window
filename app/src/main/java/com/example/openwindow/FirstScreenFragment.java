package com.example.openwindow;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.openwindow.settings.vibration.VibrationUtils;
import com.example.openwindow.settings.sound.SoundCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstScreenFragment extends Fragment {

    String errorMessage;
    String yesTitle;
    String yesMessage;
    String noTitle;
    String noMessage;
    String topText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        errorMessage = getString(R.string.error_message);
        yesTitle = getString(R.string.yes_title);
        yesMessage = getString(R.string.yes_message);
        noTitle = getString(R.string.no_title);
        noMessage = getString(R.string.no_message);
        topText = getString(R.string.yes_top_text);
        return inflater.inflate(R.layout.fragment_first_screen, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView topTextView = view.findViewById(R.id.top_text);
        ImageView headerIcon = view.findViewById(R.id.top_icon);

        TextView detailsText = view.findViewById(R.id.line_one);
        detailsText.setText(getString(R.string.line_one));

        TextView degreesText = view.findViewById(R.id.line_two);
        degreesText.setText(getString(R.string.line_two));
        TextView degreesTextView = view.findViewById(R.id.line_two_last);

        TextView imageText = view.findViewById(R.id.line_three);
        imageText.setText(getString(R.string.line_three));
        ImageView imageView = view.findViewById(R.id.line_three_last);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Document document;
                String rotationValue = null;
                try {
                    document = Jsoup.connect("https://weather.com/weather/today/l/5317f9d2d6431a7e32ed52d7d4f0813cf98bb3bf552b9a075cce6d307c316cea").get();
                    Element element = document.select("svg[data-testid=Icon][name=wind-direction]").first();
                    if (element != null) {
                        String style = element.attr("style");
                        Pattern pattern = Pattern.compile("rotate\\(([^)]+)\\)");
                        Matcher matcher = pattern.matcher(style);
                        if (matcher.find()) {
                            rotationValue = matcher.group(1);
                            String[] arr = rotationValue.split("deg");
                            rotationValue = arr[0];
                        } else {
                            rotationValue = errorMessage;
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String finalRotationValue = rotationValue;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String title = yesTitle;
                        String text = yesMessage;

                        if(Integer.parseInt(finalRotationValue) >= 270 && Integer.parseInt(finalRotationValue) < 360){
                            topText = getString(R.string.no_top_text);
                            title = noTitle;
                            text = noMessage;
                        }

                        topTextView.setText(topText);
                        degreesTextView.setText(finalRotationValue);
                        imageView.setImageResource(R.drawable.wind_arrow_down);
                        imageView.setRotation(Float.parseFloat(finalRotationValue));

                        String finalTitle = title;
                        String finalText = text;

                        headerIcon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", Context.MODE_PRIVATE);
                                boolean soundEnabled = sharedPreferences.getBoolean("sound_enabled", false);
                                if (soundEnabled) {
                                    ((SoundCallback) getActivity()).playSound();
                                }

                                boolean vibrationEnabled = sharedPreferences.getBoolean("vibration_enabled", false);
                                if (vibrationEnabled) {
                                    VibrationUtils.vibratePhone(getContext());
                                }
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(finalTitle)
                                        .setMessage(finalText)
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", Context.MODE_PRIVATE);
                                                boolean soundEnabled = sharedPreferences.getBoolean("sound_enabled", false);
                                                if (soundEnabled) {
                                                    ((SoundCallback) getActivity()).playSound();
                                                }

                                                boolean vibrationEnabled = sharedPreferences.getBoolean("vibration_enabled", false);
                                                if (vibrationEnabled) {
                                                    VibrationUtils.vibratePhone(getContext());
                                                }
                                            }
                                        })
                                        .show();
                            }
                        });
                    }
                });
            }
        }).start();
    }
}