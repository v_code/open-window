package com.example.openwindow;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

public class SettingsFragment extends PreferenceFragmentCompat {

    private SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener;
    private boolean isTurnOffButton = false;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        Preference turnOffPreference = findPreference("turn_off");
        if (turnOffPreference != null) {
            turnOffPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    isTurnOffButton = true;

                    SwitchPreferenceCompat soundPreference = findPreference("sound_enabled");
                    if (soundPreference != null) {
                        soundPreference.setChecked(false);
                    }

                    SwitchPreferenceCompat vibrationPreference = findPreference("vibration_enabled");
                    if (vibrationPreference != null) {
                        vibrationPreference.setChecked(false);
                    }

                    Toast.makeText(requireContext(), "Sound and Vibration turned off", Toast.LENGTH_SHORT).show();
                    isTurnOffButton = false;
                    return true;
                }
            });
        }

        preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

                if (key.equals("sound_enabled")) {
                    boolean enabled = sharedPreferences.getBoolean(key, false);
                    if(!isTurnOffButton){
                        Toast.makeText(requireContext(),
                                enabled ? getString(R.string.sound_on) : getString(R.string.sound_off),
                                Toast.LENGTH_SHORT).show();
                    }

                }
                if (key.equals("vibration_enabled")) {
                    boolean enabled = sharedPreferences.getBoolean(key, false);
                    if(!isTurnOffButton){
                        Toast.makeText(requireContext(),
                                enabled ? getString(R.string.vibrate_on) : getString(R.string.vibrate_off),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }
}
