package com.example.openwindow.footer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.openwindow.FirstScreenFragment;
import com.example.openwindow.ForecastFragment;
import com.example.openwindow.R;
import com.example.openwindow.settings.sound.SoundCallback;
import com.example.openwindow.settings.vibration.VibrationUtils;

public class FooterFragment extends Fragment {

    private View view;
    private ImageView btnLeft;
    private ImageView btnHome;
    private ImageView btnRight;
    private SoundCallback soundCallback;
    private SharedPreferences sharedPreferences;

    private boolean isBtnLeftClickable;
    private boolean isBtnRightClickable;

    public enum FragmentState {
        FIRST_SCREEN, FORECAST, SETTINGS
    }

    private FragmentState currentFragmentState = FragmentState.FIRST_SCREEN;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SoundCallback) {
            soundCallback = (SoundCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement SoundCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        soundCallback = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_footer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.view = view;
        btnLeft = view.findViewById(R.id.icon_one);
        btnHome = view.findViewById(R.id.icon_two);
        btnRight = view.findViewById(R.id.icon_three);

        updateIcons();
        setButtonClickers();
    }

    private void setButtonClickers() {
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", Context.MODE_PRIVATE);
                boolean soundEnabled = sharedPreferences.getBoolean("sound_enabled", false);
                if (soundEnabled) {
                    soundCallback.playSound();
                }

                boolean vibrationEnabled = sharedPreferences.getBoolean("vibration_enabled", false);
                if (vibrationEnabled) {
                    VibrationUtils.vibratePhone(getContext());
                }

                if(currentFragmentState != null) {
                    currentFragmentState = FragmentState.FIRST_SCREEN;
                    updateFragment(new FirstScreenFragment());
                }
            }
        });

        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", Context.MODE_PRIVATE);
                boolean soundEnabled = sharedPreferences.getBoolean("sound_enabled", false);
                if (isBtnRightClickable && soundEnabled) {
                    soundCallback.playSound();
                }

                boolean vibrationEnabled = sharedPreferences.getBoolean("vibration_enabled", false);
                if (vibrationEnabled) {
                    VibrationUtils.vibratePhone(getContext());
                }

                if(currentFragmentState != null) {
                    currentFragmentState = FragmentState.FORECAST;
                    updateFragment(new ForecastFragment());
                }
            }
        });

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName() + "_preferences", Context.MODE_PRIVATE);
                boolean soundEnabled = sharedPreferences.getBoolean("sound_enabled", false);
                if (isBtnLeftClickable && soundEnabled) {
                    soundCallback.playSound();
                }

                boolean vibrationEnabled = sharedPreferences.getBoolean("vibration_enabled", false);
                if (vibrationEnabled) {
                    VibrationUtils.vibratePhone(getContext());
                }

                if(currentFragmentState != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    currentFragmentState = FragmentState.FIRST_SCREEN;
                    updateIcons();
                }
            }
        });
    }

    public void updateIcons() {
        if(currentFragmentState != null) {
            switch (currentFragmentState) {
                case FIRST_SCREEN:
                    btnLeft.setVisibility(View.VISIBLE);
                    btnHome.setVisibility(View.GONE);
                    btnRight.setVisibility(View.VISIBLE);
                    setButtonResources(R.drawable.icon_arrow_left_block, R.drawable.icon_arrow_right);

                    btnLeft.setClickable(false);
                    btnRight.setClickable(true);
                    isBtnLeftClickable = false;
                    isBtnRightClickable = true;
                    break;
                case FORECAST:
                    btnLeft.setVisibility(View.VISIBLE);
                    btnHome.setVisibility(View.GONE);
                    btnRight.setVisibility(View.VISIBLE);
                    setButtonResources(R.drawable.icon_arrow_left, R.drawable.icon_arrow_right_block);

                    btnLeft.setClickable(true);
                    btnRight.setClickable(false);
                    isBtnLeftClickable = true;
                    isBtnRightClickable = false;
                    break;
                case SETTINGS:
                    btnLeft.setVisibility(View.GONE);
                    btnHome.setVisibility(View.VISIBLE);
                    btnRight.setVisibility(View.GONE);
                    setButtonResources(R.drawable.icon_home);
                    btnHome.setClickable(true);
                    break;
            }
        }
    }

    private void setButtonResources(int leftIcon, int rightIcon) {
        btnLeft.setBackgroundResource(leftIcon);
        btnRight.setBackgroundResource(rightIcon);
    }

    private void setButtonResources(int homeIcon) {
        btnHome.setBackgroundResource(homeIcon);
    }

    private void updateFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
        updateIcons();
    }

    public void setCurrentFragmentState(FragmentState state) {
        this.currentFragmentState = state;
    }
}