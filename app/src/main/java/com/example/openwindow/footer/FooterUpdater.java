package com.example.openwindow.footer;

public interface FooterUpdater {
    void updateFooterState(FooterFragment.FragmentState state);
}
