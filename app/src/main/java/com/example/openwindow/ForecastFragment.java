package com.example.openwindow;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class ForecastFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        GridLayout gridLayout = view.findViewById(R.id.forecast_grid_layout_id);

        TextView forecastMainText = view.findViewById(R.id.forecast_main_text);
        forecastMainText.setText(getString(R.string.today_forecast));

        new Thread(new Runnable() {
            @Override
            public void run() {
                Document document;
                try {
                    document = Jsoup.connect("https://weather.com/weather/hourbyhour/l/5317f9d2d6431a7e32ed52d7d4f0813cf98bb3bf552b9a075cce6d307c316cea")
                            .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36")
                            .get();

                    Elements dateElements = document.select("h3[data-testid=daypartName][class=DetailsSummary--daypartName--kbngc]");
                    Elements windElements = document.select("span[data-testid=Wind][class=Wind--windWrapper--3Ly7c DetailsTable--value--2YD0-]");
                    ArrayList<String> windList = new ArrayList<>();
                    ArrayList<String> dateList = new ArrayList<>();

                    for(Element el : dateElements) {
                        String str = el.text();
                        dateList.add(str);
                        if(str.equals("11 pm")){
                            break;
                        }
                    }
                    int numberOfHoursRemaining = dateList.size();
                    int n = 0;
                    for (Element e : windElements) {
                        if(n < numberOfHoursRemaining) {
                            String str = e.text();
                            String[] arr = str.split(" ");
                            windList.add(arr[0]);
                        }
                        n++;
                    }

                    Map<String, String> dateWindMap = new LinkedHashMap<>();
                    for (int i = 0; i < dateList.size() && i < windList.size(); i++) {
                        dateWindMap.put(dateList.get(i), windList.get(i));
                    }

                    new Handler(Looper.getMainLooper()).post(() -> {
                        for (Map.Entry<String, String> entry : dateWindMap.entrySet()) {
                            LinearLayout newLayout = new LinearLayout(getContext());
                            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
                            layoutParams.width = 0;
                            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            layoutParams.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
                            newLayout.setOrientation(LinearLayout.VERTICAL);
                            newLayout.setGravity(View.TEXT_ALIGNMENT_CENTER);
                            newLayout.setLayoutParams(layoutParams);
                            layoutParams.setMargins(0, 0, 0, 24);

                            TextView timeTextView = new TextView(getContext());
                            timeTextView.setText(entry.getKey());
                            timeTextView.setTextSize(24);
                            timeTextView.setTextColor(getResources().getColor(R.color.black));

                            TextView directionTextView = new TextView(getContext());
                            directionTextView.setText(entry.getValue());
                            directionTextView.setTextSize(24);
                            directionTextView.setTextColor(getResources().getColor(R.color.black));

                            newLayout.addView(timeTextView);
                            newLayout.addView(directionTextView);
                            gridLayout.addView(newLayout);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return view;
    }
}